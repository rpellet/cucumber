package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class storeSteps {
	
	WebDriver driver;

	@Given("I am on the jpetstore homepage")
	public void i_am_on_the_jpetstore_homepage() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	@When("I sign in with the login {string} and password {string}")
	public void i_sign_in_with_the_login_and_password(String login, String password) throws Throwable {
	    driver.findElement(By.xpath("//a[contains(text(), 'Sign In')]")).click();
	    driver.findElement(By.xpath("//input[@name='username']")).sendKeys(login);
	    driver.findElement(By.xpath("//input[@name='password']")).clear();
	    driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
	    driver.findElement(By.xpath("//input[@name='signon']")).click();
	}

	@Then("The message Welcome {string}! is displayed")
	public void the_message_Welcome_is_displayed(String username) {
		String xpath = "//div[contains(text(), 'Welcome " + username + "!')]";
		driver.findElement((By.xpath(xpath)));
//		driver.findElement((By.xpath("//div[contains(text(), 'Welcome " + username + "!')]")));
	}

}