package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetstoreSteps {
	
	WebDriver driver;
	
	@Given("I have an open browser")
	public void i_have_an_open_browser() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}

	@When("I try to access jpetstore")
	public void i_try_to_access_jpetstore() throws Throwable {
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	@Then("jpetstore homepage is displayed")
	public void jpetstore_homepage_is_displayed() throws Throwable {
	    driver.getTitle().contentEquals("JPetStore Demo");
	}

	@When("I login to the application")
	public void i_login_to_the_application() throws Throwable {
	    driver.findElement(By.xpath("//a[contains(text(), 'Sign In')]")).click();
	    driver.findElement(By.xpath("//input[@name='username']")).sendKeys("j2ee");
	    driver.findElement(By.xpath("//input[@name='password']")).clear();
	    driver.findElement(By.xpath("//input[@name='password']")).sendKeys("j2ee");
	    driver.findElement(By.xpath("//input[@name='signon']")).click();
	}

	@Then("A welcome message is displayed")
	public void a_welcome_message_is_displayed() throws Throwable {
	    driver.findElement(By.xpath("//div[contains(text(), 'Welcome ABC!')]"));
	}
	
	
}