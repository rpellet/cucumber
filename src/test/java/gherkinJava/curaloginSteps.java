package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class curaloginSteps {
	
	WebDriver driver;
	
	@Given("L'utilisateur est sur la page d'accueil")
	public void l_utilisateur_est_sur_la_page_d_accueil() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get("https://katalon-demo-cura.herokuapp.com/");
	}

	@When("L'utilisateur veux reserver un rendez-vous")
	public void l_utilisateur_veux_reserver_un_rendez_vous() throws Throwable {
		driver.findElement(By.xpath("//a[@id='btn-make-appointment']")).click();
	}

	@Then("L'utilisateur est envoye vers la page de login")
	public void l_utilisateur_est_envoye_vers_la_page_de_login() throws Throwable {
		driver.findElement(By.xpath("//button[@id='btn-login']"));
	}

	@When("L'utilisateur se connecte")
	public void l_utilisateur_se_connecte() throws Throwable {
		driver.findElement(By.xpath("//input[@id='txt-username']")).sendKeys("John Doe");
		driver.findElement(By.xpath("//input[@id='txt-password']")).sendKeys("ThisIsNotAPassword");
		driver.findElement(By.xpath("//button[@id='btn-login']")).click();
	}

	@Then("L'utilisateur est connecte sur la page de prise de rendez-vous")
	public void l_utilisateur_est_connecte_sur_la_page_de_prise_de_rendez_vous() throws Throwable {
		driver.findElement(By.xpath("//button[@id='btn-book-appointment']"));
		driver.quit();
	}
}
