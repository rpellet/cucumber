Feature: login to store
  I want to use this template for my feature file

  Scenario Outline: login to jpetstore
    Given I am on the jpetstore homepage
    When I sign in with the login "<login>" and password "<password>"
    Then The message Welcome "<username>"! is displayed

    Examples: 
      | login | password | username |
      | j2ee | j2ee | ABC |
      | ACID |ACID | ABC |
